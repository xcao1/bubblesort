#ifndef __MY_BUBBLE_SORT__
#define	__MY_BUBBLE_SORT__
// =================================
template <class DataType>
class MyBubbleSort{
	// if a is bigger than b
	static bool NoLessThan(DataType a, DataType b);
public:
	static bool BubbleSortAnArray(DataType *in_array, int size);
};
template <class DataType>
bool MyBubbleSort<DataType>::NoLessThan(DataType a, DataType b){
	if(a>=b)
		return true;
	else
		return false;
}

template <class DataType>
bool MyBubbleSort<DataType>::BubbleSortAnArray(DataType *in_array, int size){
	// n-1 sort
	// since there are n elements
	// each time move a biggest element to the last
	// if already compared, do not need to compare again
	for(int i = 0; i != size-1; i++){
		// since the n-1 elements are already compared
		// the n's element (last one) do not need to 
		// compare to these already sorted elements
		for(int j = 0; (j+1) != (size-i); j++){
			// after each turn, the sorted element increase 1
			// i's turn, there are i sorted elements
			if(NoLessThan(in_array[j],in_array[j+1])){
				// exchange the two element
				DataType temp = in_array[j+1];
				in_array[j+1] = in_array[j];
				in_array[j] = temp;
			}
		}
	}
	return true;
}

// =================================
#endif // !__MY_BUBBLE_SORT__
